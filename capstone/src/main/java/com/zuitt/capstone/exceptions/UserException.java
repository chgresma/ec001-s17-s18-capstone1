package com.zuitt.capstone.exceptions;

public class UserException extends Exception {
    public UserException(String message) {
        super(message);
    }
}
