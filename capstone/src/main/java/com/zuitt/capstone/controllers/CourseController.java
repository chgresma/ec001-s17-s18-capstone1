package com.zuitt.capstone.controllers;

import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CourseController {

    @Autowired
    private CourseService courseService;

    @RequestMapping(value = "/courses", method = RequestMethod.POST)
    public ResponseEntity<Object> createCourse(@RequestBody Course course, @RequestHeader("Authorization") String stringToken) {
        courseService.createCourse(course, stringToken);
        return new ResponseEntity<>("Course created successfully.", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/courses", method = RequestMethod.GET)
    public ResponseEntity<Object> getCourses() {
        return new ResponseEntity<>(courseService.getCourses(), HttpStatus.OK);
    }

    @RequestMapping(value = "/courses/active", method = RequestMethod.GET)
    public ResponseEntity<Object> getActiveCourses() {
        return new ResponseEntity<>(courseService.getActiveCourses(), HttpStatus.OK);
    }

    @RequestMapping(value = "/courses/{courseId}", method = RequestMethod.GET)
    public ResponseEntity<Object> getCourse(@PathVariable int courseId) {
        return new ResponseEntity<>(courseService.getCourse(courseId), HttpStatus.OK);
    }

    @RequestMapping(value = "/courses/{courseId}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateCourse(@PathVariable int courseId, @RequestBody Course course, @RequestHeader("Authorization") String stringToken) {
        return courseService.updateCourse(courseId, course, stringToken);
    }

    @RequestMapping(value = "/courses/{courseId}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteCourse(@PathVariable int courseId, @RequestHeader("Authorization") String stringToken) {
        return courseService.deleteCourse(courseId, stringToken);
    }

    @RequestMapping(value = "/courses/archive-unarchive/{courseId}", method = RequestMethod.PUT)
    public ResponseEntity<Object> archiveUnarchiveCourse(@PathVariable int courseId, @RequestHeader("Authorization") String stringToken) {
        return courseService.archiveUnarchiveCourse(courseId, stringToken);
    }
}