package com.zuitt.capstone.controllers;

import com.zuitt.capstone.services.CourseEnrollmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CourseEnrollmentController {

    @Autowired
    private CourseEnrollmentService courseEnrollmentService;

    @RequestMapping(value = "/enroll/{courseId}", method = RequestMethod.POST)
    public ResponseEntity<Object> enrollCourse(@RequestHeader("Authorization") String stringToken, @PathVariable int courseId) {
        return new ResponseEntity<>(courseEnrollmentService.enrollCourse(stringToken, courseId), HttpStatus.OK);
    }
}