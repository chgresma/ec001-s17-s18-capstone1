package com.zuitt.capstone.services;

import com.zuitt.capstone.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseEnrollmentService {

    String enrollCourse(String stringToken, int courseId);
}