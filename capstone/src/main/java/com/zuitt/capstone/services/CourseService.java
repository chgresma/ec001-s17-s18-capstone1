package com.zuitt.capstone.services;

import com.zuitt.capstone.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseService {

    void createCourse(Course course, String stringToken);

    Course getCourse(int courseId);

    Iterable<Course> getCourses();

    Iterable<Course> getActiveCourses();

    ResponseEntity updateCourse(int id, Course course, String stringToken);

    ResponseEntity deleteCourse(int id, String stringToken);

    ResponseEntity archiveUnarchiveCourse(int courseId, String stringToken);

}