package com.zuitt.capstone.services;

import com.zuitt.capstone.config.JwtToken;
import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.models.CourseEnrollment;
import com.zuitt.capstone.models.User;
import com.zuitt.capstone.repositories.CourseEnrollmentRepository;
import com.zuitt.capstone.repositories.CourseRepository;
import com.zuitt.capstone.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;

@Service
public class CourseEnrollmentServiceImpl implements CourseEnrollmentService {

    @Autowired
    private CourseEnrollmentRepository courseEnrollmentRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private JwtToken jwtToken;

    @Override
    public String enrollCourse(String stringToken, int courseId) {
        User user = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Course course = courseRepository.findById(courseId).get();
        CourseEnrollment courseEnrollment = new CourseEnrollment();

        if(course.isActive()) {
            courseEnrollment.setCourse(course);
            courseEnrollment.setUser(user);
            courseEnrollment.setDateTimeEnrolled(LocalDateTime.now());

            courseEnrollmentRepository.save(courseEnrollment);

            return "Enrollment successful";
        } else {
            return "Course unarchived. You cannot enroll to this course.";
        }
    }
}